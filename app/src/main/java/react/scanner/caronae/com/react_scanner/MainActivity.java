package react.scanner.caronae.com.react_scanner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.scanlibrary.ScanConstants;
import com.scanlibrary.ScanStarter;
import com.scanlibrary.Utils;

import java.io.IOException;

import static com.scanlibrary.Utils.REQUEST_CODE;

public class MainActivity extends AppCompatActivity {

    private Button btn_start_scan, btn_getBase64;
    private ScanStarter scanStarter;
    private Bitmap bitmap;
    private ProgressDialog progressDialog;
    private TextView tv_base64;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // full screen activity
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // hide action bar
        if(getSupportActionBar() != null){
            getSupportActionBar().hide();
        }

        intit();

        btn_start_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scanStarter != null){
                    scanStarter.start();
                }
                else{
                    Toast.makeText(MainActivity.this, "Something is wrong !!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_getBase64.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(bitmap != null){
                    // show progress dialog
                    progressDialog.show();

                    String base64 = scanStarter.encodeTobase64(bitmap);
                    tv_base64.setText("Base 64: " + base64.substring(0, 20) + " ...");

                    // get base 64 result
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });

                    // dismiss progress dialog
                    progressDialog.dismiss();
                }
                else{
                    Toast.makeText(MainActivity.this, "Bitmap not available yet !", Toast.LENGTH_SHORT).show();
                }



            }
        });


    }

    /**
     * Initialize UI componnents
     */
    private void intit() {
        // btn scan activity
        btn_start_scan = findViewById(R.id.btn_start_scan);
        // btn get base 64
        btn_getBase64 = findViewById(R.id.btn_get_base64);
        // tv base64 result
        tv_base64 = findViewById(R.id.tv_base64);
        // scan starter
        scanStarter = new ScanStarter(this);
        // bitmap result from scan activity
        bitmap = null;

        // progress dialog configuration
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Veuillez patienter ...");
        progressDialog.setTitle("CARONAE Systems");

    }

    //Once the scanning is done, the application is returned from scan library to main app, to retrieve the scanned image,
    // add onActivityResult in your activity or fragment from where you have started startActivityForResult,
    // below is the sample code snippet:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            // show progress bar
            progressDialog.show();

            // here we get the unified resource dientifier of the scanned image
            Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);

            try {
                // here we get our scanned bitmap
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                getContentResolver().delete(uri, null, null);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // dismiss progress dialog
            progressDialog.dismiss();

        }
    }

}
